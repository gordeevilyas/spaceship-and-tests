package tests;

import itis.quiz.spaceships.CommandCenter;
import itis.quiz.spaceships.Spaceship;
import itis.quiz.spaceships.SpaceshipFleetManager;
import org.junit.jupiter.api.*;

import java.util.ArrayList;

public class SpaceshipFleetManagerTestJunit
{
    SpaceshipFleetManager commandCenter = new CommandCenter();
    static ArrayList<Spaceship> listOfSpaceships = new ArrayList<>();

    @BeforeEach
    void fillArr()
    {
        listOfSpaceships.add(new Spaceship("Foo", 100, 100, 0));
        listOfSpaceships.add(new Spaceship("Bar", 100, 50, 0));
        listOfSpaceships.add(new Spaceship("Boom", 0 ,0 ,0));
    }
    @AfterEach
    void clearArr()
    {
        listOfSpaceships.clear();
    }



    @DisplayName("Точно ли возвращает самый powerful")
    @Test
    void getMostPowerfulShip_isReturnsMostPowerful()
    {

        listOfSpaceships.remove(0);
        Spaceship returnsMostPowerful = commandCenter.getMostPowerfulShip(listOfSpaceships);
        Assertions.assertEquals("Bar", returnsMostPowerful.getName());
    }

    @DisplayName("Точно ли возвращает первый из самых powerful")
    @Test
    void getMostPowerfulShip_isReturnsFirstOfPowerfulShips()
    {

        Spaceship returnsFirstOfPowerfulShips = commandCenter.getMostPowerfulShip(listOfSpaceships);
        Assertions.assertEquals("Foo", returnsFirstOfPowerfulShips.getName());
    }

    @DisplayName("Возврат null, если все корабли мирные")
    @Test
    void getMostPowerfulShip_isReturnsNullWhenThereAreNoMatchingShips()
    {

        listOfSpaceships.remove(0);
        listOfSpaceships.remove(0);
        Spaceship returnsNullWhenThereAreNoMatchingShips = commandCenter.getMostPowerfulShip(listOfSpaceships);
        Assertions.assertNull(returnsNullWhenThereAreNoMatchingShips);
    }



    @DisplayName("Возврат корабля по имени")
    @Test
    void getShipByName_isReturnsShipByName()
    {

        Spaceship foo = commandCenter.getShipByName(listOfSpaceships, "Foo");
        Assertions.assertEquals("Foo", foo.getName());

    }

    @DisplayName("Возврат null, если у корабля нет имени")
    @Test
    void getShipByName_isReturnsNullWhenThereAreNoMatchingShips()
    {

        Spaceship returnsNullWhenThereAreNoMatchingShips = commandCenter.getShipByName(listOfSpaceships, "Boom");
        Assertions.assertNull(returnsNullWhenThereAreNoMatchingShips);
    }




    @DisplayName("Точно ли возвращает все корабли с достаточным грузовым отсеком")
    @Test
    void getAllShipsWithEnoughCargoSpace_isReturnsAllShipsWithEnoughCargoSpace()
    {

        ArrayList<Spaceship> returnsAllShipsWithEnoughCargoSpace = commandCenter.getAllShipsWithEnoughCargoSpace(listOfSpaceships, 90);
        boolean isTrue = true;
        for (Spaceship spaceship : returnsAllShipsWithEnoughCargoSpace)
        {
            if (spaceship.getCargoSpace() < 90)
            {
                isTrue = false;
                break;
            }
        }
    }
    @DisplayName("Возврат null, если нет кораблей с достаточным размером грузового отсека")
    @Test
    void getAllShipsWithEnoughCargoSpace_isReturnsNullWhenThereAreNoShipsWithEnoughCargoSpace()
    {

        ArrayList<Spaceship> returnsNullWhenThereAreNoShipsWithEnoughCargoSpace = commandCenter.getAllShipsWithEnoughCargoSpace(listOfSpaceships, 110);
        Assertions.assertEquals(returnsNullWhenThereAreNoShipsWithEnoughCargoSpace.size(), 0);
    }




    @DisplayName("Возврат всех мирных кораблей")
    @Test
    void getAllCiviliansShips_isReturnsAllCiviliansShips()
    {


        ArrayList<Spaceship> returnsAllCiviliansShips = commandCenter.getAllCivilianShips(listOfSpaceships);
        boolean isTrue = true;
        for (Spaceship spaceship : returnsAllCiviliansShips)
        {
            if (spaceship.getFirePower() > 0)
            {
                isTrue = false;
                break;
            }
        }
        Assertions.assertTrue(isTrue);
    }

    @DisplayName("Возврат null, если нет мирных кораблей")
    @Test
    void getAllCiviliansShips_isReturnNullWhenThereAreNoMatchingShips()
    {

        listOfSpaceships.remove(2);
        ArrayList<Spaceship> returnsNullWhenThereAreNoMatchingShips = commandCenter.getAllCivilianShips(listOfSpaceships);
        Assertions.assertEquals(returnsNullWhenThereAreNoMatchingShips.size(), 0);
    }
}
